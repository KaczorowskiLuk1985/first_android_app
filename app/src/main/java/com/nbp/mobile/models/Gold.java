package com.nbp.mobile.models;

import com.google.gson.annotations.SerializedName;

//test gita

public class Gold {

    @SerializedName("data")
    private String date;
    @SerializedName("cena")
    private float price;

    public String getDate() {
        return date;
    }

    public float getPrice() {
        return price;
    }
}
