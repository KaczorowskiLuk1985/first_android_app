package com.nbp.mobile.models;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nbp.mobile.R;

import java.util.List;

//recyclerView dziala jak schody ruchome
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.Row> {
    private List<Rate> rates;

    public CurrencyAdapter(List<Rate> rates) {
        this.rates = rates;
    }

    @NonNull
    @Override
    public Row onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.currency_row, viewGroup, false);

        return new Row(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Row row, int i) {
        Rate rate = rates.get(i);

        row.bind(rate);
    }

    @Override
    public int getItemCount() {
        return rates.size();
    }

    class Row extends RecyclerView.ViewHolder {
        //elementy składowe wiersza
        private TextView name;
        private TextView description;
        private TextView mid;


        public Row(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);
            mid = itemView.findViewById(R.id.mid);
        }

        void bind(Rate rate) {
            name.setText(rate.getCode());
            description.setText(rate.getName());
            mid.setText(String.format("%f", rate.getMid()));
        }
    }

}