package com.nbp.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nbp.mobile.models.CurrencyAdapter;
import com.nbp.mobile.models.NbpApi;
import com.nbp.mobile.models.Rate;
import com.nbp.mobile.models.Table;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyActivity extends AppCompatActivity {

    ProgressBar progressBar;

    //metoda cyklu życia aplikacji
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
        //pobranie danych
        fetchData();
        //ustawienie toolbara
        setToolbar();
    }

    private void setToolbar() {
        //sprawdzenie
        if (getSupportActionBar() != null) {
            // i ustawienie tytułu
            getSupportActionBar().setTitle("KURSY WALUT");

            //dodanie obsługi strzałki wstecz
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //obsluga klikniecia strzałki górnej. można użyć też switcha
        if (item.getItemId() == android.R.id.home) {
            //zamkniecie obecnej aktywności.
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    //metoda pobierajaca
    private void fetchData() {
        Call<List<Table>> call = NbpApi.getApi().getCurrencies();
        call.enqueue(getCallBack());
    }

    private Callback<List<Table>> getCallBack() {
        return new Callback<List<Table>>() { // nowa klasa anonimowa
            @Override
            public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {

                //sprawdzenie czy kod odpowiedzi jest poprawny
                //tzn w zakresie 200 - 299 (kod powodzenia)
                if (response.isSuccessful()
                        && response.body() != null
                        && !response.body().isEmpty()) {

                    //wyświetlenie danych
                    showData(response.body().get(0));
                }
            }

            @Override
            public void onFailure(Call<List<Table>> call, Throwable t) {


                t.printStackTrace();
                String messege = t.getMessage();

                //TODO uzupełnić
                Toast.makeText(
                        CurrencyActivity.this,
                        messege,
                        Toast.LENGTH_SHORT)
                        .show();

            }
        };
    }


    private void showData(Table table) {
        //połączenie z widokiem
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        CurrencyAdapter adapter = new CurrencyAdapter(table.getRates());
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration divider =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(divider);
    }

}
