package com.nbp.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.nbp.mobile.models.Gold;
import com.nbp.mobile.models.NbpApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoldActivity extends AppCompatActivity {
    List<Integer> days = Arrays.asList(
            10, 20, 30, 50, 80, 90
    );

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gold);
        setSpiner();
    }

    private void setSpiner() {
        //uzyskujemy dostęęp do rozwijanej kontrolki
        Spinner spinner = findViewById(R.id.spiner);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item,
                days
        );
        //wywołąnie
        spinner.setAdapter(adapter);
        //dodanie raakcji na wybór elementu ze spinera
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fetchData(days.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void fetchData(Integer numberOdElements) {
        Call<List<Gold>> call = NbpApi.getApi().getGold(numberOdElements);
        call.enqueue(getCallback());
    }

    private Callback<List<Gold>> getCallback() {
        return new Callback<List<Gold>>() {
            @Override
            public void onResponse(Call<List<Gold>> call, Response<List<Gold>> response) {
                showData(response.body());
            }

            @Override
            public void onFailure(Call<List<Gold>> call, Throwable t) {
                t.printStackTrace();
            }
        };
    }

    private void showData(List<Gold> goldRates) {
        LineChart chart = findViewById(R.id.chart);
        //czyszczenie wykresu. czasem dane moga sie bez tego nakładać
        chart.clear();

        ArrayList<Entry> values = new ArrayList<>();
        for (int position = 0; position < goldRates.size(); position++) {
            Entry entry = new Entry(position,
                    goldRates.get(position).getPrice());

            values.add(entry);
        }
        LineDataSet set = new LineDataSet(values, "kursy złota");
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setCircleColor(getResources().getColor(R.color.colorPrimary));
        set.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
        set.setDrawFilled(true); //wypełnienie pola
        set.setDrawCircles(false);
        set.setLineWidth(5f);

        //obiekt klasy LineData
        LineData data = new LineData(set);
        chart.setData(data);
        chart.invalidate();
    }
}
