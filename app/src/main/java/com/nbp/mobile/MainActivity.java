package com.nbp.mobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        setupUi();
    }

    private void setupUi() {
        Button first = findViewById(R.id.first_button);

        Button second = findViewById(R.id.second_button);
        //dodanie listenera reagujacego na krotkie klikniecie
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GoldActivity.class);
                //otwarcie nowej aktywności
                startActivity(intent);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intet = new Intent(MainActivity.this,
                        CurrencyActivity.class);

                startActivity(intet);
            }
        });
    }
}
